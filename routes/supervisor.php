<?php

use Illuminate\Support\Facades\Route;


Route::group(['middleware'=>['guest:supervisor'],'as'=>'supervisor.'],function (){
    Route::get('/login', 'Auth\LoginController@create')->name('login');
    Route::post('/login', 'Auth\LoginController@store')->name('login.store');
});


Route::group(['middleware'=>['auth:supervisor'],'as'=>'supervisor.'],function (){
    Route::post('/logout', 'Auth\LoginController@destroy')->name('logout');
    Route::get('/','HomeController@index')->name('home');
    Route::resource('categories','CategoriesController');
    Route::delete('delete-selected/categories','CategoriesController@delete_selected')->name('categories.delete_selected');
    Route::resource('products','ProductsController');
    Route::delete('delete-selected/products','ProductsController@delete_selected')->name('products.delete_selected');
    Route::delete('img-delete/products/{id}','ProductsController@imgDelete')->name('products.delImage');

});


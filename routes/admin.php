<?php

use Illuminate\Support\Facades\Route;


Route::group(['middleware'=>['guest:admin'],'as'=>'admin.'],function (){
    Route::get('/login', 'Auth\LoginController@create')->name('login');
    Route::post('/login', 'Auth\LoginController@store')->name('login.store');
});


Route::group(['middleware'=>['auth:admin'],'as'=>'admin.'],function (){
    Route::post('/logout', 'Auth\LoginController@destroy')->name('logout');
    Route::get('/','HomeController@index')->name('home');
    Route::resource('supervisor','SupervisorController');
    Route::delete('delete-selected/supervisor','SupervisorController@delete_selected')->name('supervisor.delete_selected');
    Route::patch('active-toggle/supervisor/{supervisor}','SupervisorController@activeToggle')->name('supervisor.activeToggle');
});


@extends('supervisor.layouts.app')
@section('title' , 'تفاصيل المنتج')
@section('breadcrumb')
    @php
        $routes =  [
    ['route'=>route('supervisor.products.index'),'name'=>'المنتجات']
   ];
    @endphp
    @include('supervisor.layouts.page-breadcrumb',$routes)
@endsection
@section('content')
    <div class="row">
        <!-- column -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body box bg-cyan ">
                    <h4 class="card-title text-white">تفاصيل المنتج</h4>
                </div>

                <div class="accordion" id="accordionExample">
                    <div id="collapse1" class="collapse show" aria-labelledby="heading1" data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="row">
                                @forelse($product->images as $img)
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="card">
                                            <div class="comment-widgets">
                                                <div class="card-body">
                                                    <img src="{{getImg($img->image)}}" loading="lazy" alt="user" class="hai-img" width="100" height="100">
                                                </div>
                                            </div>
                                            <button class="btn btn-outline-danger btn-sm btn-rounded" onclick="event.preventDefault(); document.getElementById('del-img-{{$img->id}}').submit();">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            <form id="del-img-{{$img->id}}" action="{{ route('supervisor.products.delImage',$img->id) }}" method="post" class="d-none">
                                                {{method_field('delete')}}
                                                @csrf
                                            </form>
                                        </div>
                                    </div>
                                @empty
                                    <div class="card">
                                        <h5 class="text-center">لا توجد صور</h5>
                                    </div>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

    </script>
@endsection

<div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body box bg-cyan ">
                <h4 class="card-title text-white">بيانات المنتج</h4>
            </div>
            <div class="comment-widgets">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="name" class="col-sm-1 text-right control-label col-form-label">الاسم </label>
                        <div class="col-sm-5">
                            {!! Form::text('name',null,['class' =>'form-control '.($errors->has('name') ? ' is-invalid' : null),'placeholder'=> 'الاسم ','id'=>'name' ]) !!}
                            @error('name')
                            <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <label for="slug" class="col-sm-1 text-left control-label col-form-label">المعرف (slug) </label>
                        <div class="col-sm-5">
                            {!! Form::text('slug',null,['class' =>'form-control '.($errors->has('slug') ? ' is-invalid' : null),'placeholder'=> 'معرف (slug) ','id'=>'slug' ]) !!}
                            @error('slug')
                            <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="category_id" class="col-sm-1 text-right control-label col-form-label">القسم </label>
                        <div class="col-sm-5">
                            {!! Form::select("category_id",categories(),null,['class' =>'form-control '.($errors->has('category_id') ? ' is-invalid' : null),
                                'data-show-subtext'=>'true','data-live-search'=>'true','id' => 'category_id','placeholder'=>'اختر القسم '])!!}
                            @error('category_id')
                                <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>


                        <label for="description" class="col-sm-1 text-left control-label col-form-label">الوصف </label>
                        <div class="col-sm-5">
                            {!! Form::textarea('description',null,['class' =>'form-control '.($errors->has('description') ? ' is-invalid' : null),'placeholder'=> 'الوصف ','id'=>'description','height'=>'300' ]) !!}
                            @error('description')
                                <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                    </div>

                    <div class="form-group row">
                        <label for="image" class="col-sm-1 text-right control-label col-form-label">الصورة </label>
                        <div class="col-sm-5">
                            {!! Form::file('image',['class'=>'form-control '.($errors->has('image') ? ' is-invalid' : null),'id'=>'image']) !!}
                            @error('image')
                            <span class="invalid-feedback display-img-validation">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <label for="image" class="col-sm-1 text-left control-label col-form-label">الصورة </label>
                        <div class="col-sm-5">
                            <img src="{{isset($product)?$product->image:asset('_admin/assets/images/user.jpg')}}" alt="user" class="img-thumbnail" height="70" width="70">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="images" class="col-sm-1 text-right control-label col-form-label">الصور </label>
                        <div class="col-sm-5">
                            {!! Form::file('images[]',['class'=>'form-control '.($errors->has('images.*') ? ' is-invalid' : null),'id'=>'images','multiple']) !!}
                            @error('images.*')
                            <span class="invalid-feedback display-img-validation">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <label for="images" class="col-sm-1 text-left control-label col-form-label">الصور </label>
                        <div class="col-sm-5">
                            @if(isset($product))
                                @forelse($product->images as $item)
                                    <img src="{{getImg($item->image)}}" alt="user" class="img-thumbnail" height="70" width="70">
                                @empty
                                @endforelse
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>


</div>
<div class="border-top">
    <div class="card-body text-center">
        <button type="submit" class="btn btn-primary">تأكيد</button>
    </div>
</div>


@extends('supervisor.layouts.app')
@section('title' , 'بيانات المنتج')
@section('styles')
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet"
@endsection
@section('breadcrumb')
    @php
        $routes =  [
            ['route'=>route('supervisor.products.index'),'name'=>'المنتجات']
        ];
    @endphp
    @include('supervisor.layouts.page-breadcrumb',$routes)
@endsection
@section('content')
    {!! Form::model($product,['route' =>['supervisor.products.update',$product->id],'method'=>'put','class'=>'form-horizontal','files'=>true]) !!}
        @include('supervisor.products.form')
    {!! Form::close() !!}
@endsection
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#description').summernote({
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ],
                "height":"100px"
            });
        });
    </script>
@endsection

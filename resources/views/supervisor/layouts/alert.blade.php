
@if(Session::has('success'))
    <div class="alert alert-success alert-styled-left">
        <button type="button" class="close" data-dismiss="alert">
            <span>×</span>
            <span class="sr-only">Close</span>
        </button>
        {{ Session::get('success') }}
    </div>
@endif

@if(Session::has('alert'))
    <div class="alert alert-info alert-styled-left">
        <button type="button" class="close" data-dismiss="alert">
            <span>×</span>
            <span class="sr-only">Close</span>
        </button>
        {{ Session::get('alert') }}
    </div>
@endif

@if(Session::has('error'))
    <div class="alert alert-danger alert-styled-left d-block">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
        {{ Session::get('error') }}
    </div>
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

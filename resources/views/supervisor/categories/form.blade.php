<div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body box bg-cyan ">
                <h4 class="card-title text-white">بيانات القسم</h4>
            </div>
            <div class="comment-widgets">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="name" class="col-sm-1 text-right control-label col-form-label">الاسم </label>
                        <div class="col-sm-5">
                            {!! Form::text('name',null,['class' =>'form-control '.($errors->has('name') ? ' is-invalid' : null),'placeholder'=> 'الاسم ','id'=>'name' ]) !!}
                            @error('name')
                            <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <label for="slug" class="col-sm-1 text-left control-label col-form-label">معرف (slug) </label>
                        <div class="col-sm-5">
                            {!! Form::text('slug',null,['class' =>'form-control '.($errors->has('slug') ? ' is-invalid' : null),'placeholder'=> 'معرف (slug)  ','id'=>'slug' ]) !!}
                            @error('slug')
                            <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-sm-1 text-right control-label col-form-label">الايقون </label>
                        <div class="col-sm-5">
                            <div class="btn-group">
                                <button data-selected="graduation-cap" type="button" class="icp demo btn btn-default dropdown-toggle iconpicker-component" data-toggle="dropdown">
                                    Dropdown  <i class="fa fa-fw"></i>
                                    <span class="caret"></span>
                                </button>
                                <div class="dropdown-menu"></div>
                            </div>
                            <input type="hidden" name="icon" id="icon">
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>


</div>
<div class="border-top">
    <div class="card-body text-center">
        <button type="submit" class="btn btn-primary">تأكيد</button>
    </div>
</div>


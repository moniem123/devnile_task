@extends('supervisor.layouts.app')
@section('title' , 'بيانات الاقسام')
@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fontawesome-iconpicker/3.2.0/css/fontawesome-iconpicker.min.css"/>
@endsection
@section('breadcrumb')
    @php
        $routes =  [
            ['route'=>route('supervisor.categories.index'),'name'=>'الاقسام']
        ];
    @endphp
    @include('supervisor.layouts.page-breadcrumb',$routes)
@endsection
@section('content')
    {!! Form::model($category,['route' =>['supervisor.categories.update',$category->id],'method'=>'put','class'=>'form-horizontal']) !!}
        @include('supervisor.categories.form')
    {!! Form::close() !!}
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fontawesome-iconpicker/3.2.0/js/fontawesome-iconpicker.min.js"></script>

    <script>
        $('.demo').iconpicker();
        $('.dropdown-menu').on('click','a',function (){
            $('#icon').val($(this).attr('title').replace('.','')??'')
        })
    </script>
@endsection

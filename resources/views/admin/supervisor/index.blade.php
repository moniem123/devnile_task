@extends('admin.layouts.app')
@section('title' , 'كل المشرفين')
@section('styles')
    <style>
        .active{
            color: gold;
        }
        .inactive{
            color: #0b0b0b;
        }
    </style>
@endsection
@section('breadcrumb')
    @php
        $routes =  [];
    @endphp
    @include('admin.layouts.page-breadcrumb',$routes)
@endsection
@section('content')
    <div class="row">
        <!-- column -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body box bg-cyan ">
                    <h4 class="card-title text-white">كل المشرفين</h4>
                </div>
                <div class="border-bottom">
                    <div class="card-body">
                        <a href="{{route('admin.supervisor.create')}}" class="btn btn-info">إضافة مشرف</a>
                        <button data-action="{{route('admin.supervisor.delete_selected')}}" class="btn btn-danger" id="del-all">حذف المحدد</button>
                    </div>

                </div>
                <div class="accordion" id="accordionExample">
                    <div id="collapse1" class="collapse show" aria-labelledby="heading1" data-parent="#accordionExample">
                        <div class="card-body">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead class="bg-cyan text-white">
                                <tr class="text-center">
                                    <th>#</th>
                                    <th>
                                        <input type="checkbox" name="select_all[]" id="select_all">
                                    </th>
                                    <th>الاسم</th>
                                    <th>الايميل</th>
                                    <th>الهاتف</th>
                                    <th>صورة</th>
                                    <th class="text-center">التحكم</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @forelse($supervisors as $key => $supervisor)
                                        <tr class="text-center" id="row-{{$supervisor->id}}">
                                            <td>{{++$key}}</td>
                                            <td>
                                                <input type="checkbox" value="{{$supervisor->id}}" name="select_row[]" class="select_row">
                                            </td>
                                            <td>{{$supervisor->name}}</td>
                                            <td>{{$supervisor->email}}</td>
                                            <td>{{$supervisor->phone}}</td>
                                            <td>
                                                <img src="{{$supervisor->avatar}}" alt="{{$supervisor->name}}" class="img-thumbnail" height="70px" width="70px" loading="lazy">
                                            </td>

                                            <td class="text-center">
                                                <a href="{{route('admin.supervisor.edit',[$supervisor->id])}}" class="btn btn-outline-cyan btn-rounded btn-sm">
                                                    <i class="fas fa-edit"></i>
                                                </a>

                                                <button data-href="{{route('admin.supervisor.activeToggle',[$supervisor->id])}}" class="btn btn-outline-cyan btn-rounded btn-sm activeToggle">
                                                    <i class="fas fa-star {{$supervisor->is_active?'active':'inactive'}}" ></i>
                                                </button>
                                                <a class="btn btn-outline-danger btn-rounded btn-sm btnDelete"
                                                   data-id="{{$supervisor->id}}" id='delete-form{{$supervisor->id}}'
                                                   data-action="{{route('admin.supervisor.destroy',$supervisor->id)}}">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @empty
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="border-top">
                    <div class="card-body text-center">
                        {{$supervisors->links("pagination::bootstrap-4")}}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('adminpanel/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script>
        $('.table').DataTable({
            "bPaginate": false,
            "language": {
                "paginate": {
                    "previous": "السابق",
                    "next": "التالي",
                    "search": "البحث:",
                }
            },
            "oLanguage": {
                "sSearch": "البحث:",
            },
            stateSave: true
        });
        $('#zero_config_filter').parent().prev('div').remove();
    </script>
    <script>
        $(document).ready(function (){
            $('#select_all').click(function(event) {
                if(this.checked) {
                    // Iterate each checkbox
                    $('input[name^="select_row"]').each(function() {
                        this.checked = true;
                    });
                } else {
                    $('input[name^="select_row"]').each(function() {
                        this.checked = false;
                    });
                }
            });

            $('#del-all').click(function (){
                let ids = $('input[name^="select_row"]:checked').map((i,e)=>e.value).toArray();
                let url = $(this).data('action');
                if(ids.length) {
                    customSwal(ids, url);
                }else{
                    swal("تنبية!", "قم بتحديد العناصر اولا", "info");
                }
            })

            $('.btnDelete').click(function (){
                let id = $(this).data('id');
                let url = `/admin/supervisor/${id}`;
                customSwal(id,url,$(this));
            })
            $('.activeToggle').click(function (){
                let url = $(this).data('href');
                $.ajax({
                    url,
                    type:'PATCH',
                    headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
                    success:(data)=>{
                        if(data.status){
                            let El = $(this).find('i');
                            data.flag?El.removeClass('inactive').addClass('active'):El.removeClass('active').addClass('inactive');
                            swal('تم التعديل بنجاح',data.message,'success')
                        }
                    }
                })
            })
            function customSwal(id,url,el){
                swal({
                    title: "هل أنت متاكد",
                    text: "اذا تم الحذف لا يمكن الاستعادة مرة اخرى",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url,
                            type:'DELETE',
                            data:{ids:id??''},
                            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
                            success:(data)=>{
                                if(data.status){
                                    if(el!=null){
                                        el.closest('tr').remove();
                                    }else{
                                       id.map((i)=>{
                                           $(`#row-${i}`).remove();
                                       })
                                    }
                                    swal(data.message, {icon: "success"});
                                }

                            }
                        })
                    } else {
                        swal("تم الالغاء بنجاح !");
                    }
                });
            }
        })
    </script>
@endsection

<div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body box bg-cyan ">
                <h4 class="card-title text-white">بيانات المشرف</h4>
            </div>
            <div class="comment-widgets">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="name" class="col-sm-1 text-right control-label col-form-label">الاسم </label>
                        <div class="col-sm-5">
                            {!! Form::text('name',null,['class' =>'form-control '.($errors->has('name') ? ' is-invalid' : null),'placeholder'=> 'الاسم ','id'=>'name' ]) !!}
                            @error('name')
                            <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <label for="email" class="col-sm-1 text-left control-label col-form-label">الايميل  </label>
                        <div class="col-sm-5">
                            {!! Form::email('email',null,['class' =>'form-control '.($errors->has('email') ? ' is-invalid' : null),'placeholder'=> 'الايميل','id'=>'email' ]) !!}
                            @error('email')
                            <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="phone" class="col-sm-1 text-right control-label col-form-label">الهاتف </label>
                        <div class="col-sm-5">
                            {!! Form::tel('phone',null,['class' =>'form-control '.($errors->has('phone') ? ' is-invalid' : null),'placeholder'=> 'الهاتف ','id'=>'phone' ]) !!}
                            @error('phone')
                            <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <label for="avatar" class="col-sm-1 text-left control-label col-form-label">الصورة </label>
                        <div class="col-sm-3">
                            {!! Form::file('avatar',['class'=>'form-control '.($errors->has('avatar') ? ' is-invalid' : null),'id'=>'avatar']) !!}
                            @error('avatar')
                            <span class="invalid-feedback display-img-validation">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-sm-2">
                            <img src="{{isset($supervisor)?$supervisor->avatar:asset('_admin/assets/images/user.jpg')}}" alt="user" class="img-thumbnail" height="70" width="70">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-sm-1 text-right control-label col-form-label">كلمة المرور </label>
                        <div class="col-sm-5">
                            {!! Form::password('password',['class' =>'form-control '.($errors->has('password') ? ' is-invalid' : null),'placeholder'=> 'كلمة المرور  ','id'=>'password' ]) !!}
                            @error('password')
                            <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <label for="password_confirmation" class="col-sm-1 text-left control-label col-form-label">تاكيد كلمة المرور  </label>
                        <div class="col-sm-5">
                            {!! Form::password('password_confirmation',['class' =>'form-control '.($errors->has('password_confirmation') ? ' is-invalid' : null),'placeholder'=> 'تاكيد كلمة المرور  ','id'=>'password_confirmation' ]) !!}
                            @error('password_confirmation')
                            <span class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>


</div>
<div class="border-top">
    <div class="card-body text-center">
        <button type="submit" class="btn btn-primary">تأكيد</button>
    </div>
</div>


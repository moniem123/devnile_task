@extends('admin.layouts.app')
@section('title' , 'بيانات المشرف')
@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fontawesome-iconpicker/3.2.0/css/fontawesome-iconpicker.min.css"/>
@endsection
@section('breadcrumb')
    @php
        $routes =  [
            ['route'=>route('admin.supervisor.index'),'name'=>'المشرفين']
        ];
    @endphp
    @include('admin.layouts.page-breadcrumb',$routes)
@endsection
@section('content')
    {!! Form::model($supervisor,['route' =>['admin.supervisor.update',$supervisor->id],'method'=>'put','class'=>'form-horizontal','files'=>true]) !!}
        @include('admin.supervisor.form')
    {!! Form::close() !!}
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fontawesome-iconpicker/3.2.0/js/fontawesome-iconpicker.min.js"></script>

    <script>
        $('.demo').iconpicker();
        $('.dropdown-menu').on('click','a',function (){
            $('#icon').val($(this).attr('title').replace('.','')??'')
        })
    </script>
@endsection

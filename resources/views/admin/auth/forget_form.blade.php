<div id="recoverform">
    <div class="text-center">
        <span class="text-white">أدخل عنوان بريدك الإلكتروني أدناه وسنرسل لك تعليمات حول كيفية استعادة كلمة المرور.</span>
    </div>
    <div class="row m-t-20">
        <!-- Form -->
        <form class="col-12" method="POST" action="{{--route('admin.forget')--}}">
                @csrf

            <!-- email -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-email"></i></span>
                </div>
                <input id="forgetemail" type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus placeholder="البريد الإلكتروني">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <!-- pwd -->
            <div class="row m-t-20 p-t-20 border-top border-secondary">
                <div class="col-12">
                    <a class="btn btn-info float-left" href="#" id="to-login" >الرجوع لتسجيل الدخول</a>
                    <button class="btn btn-success" type="submit" >إرسال</button>
                </div>
            </div>
        </form>
    </div>
</div>

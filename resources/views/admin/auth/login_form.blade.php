<div id="loginform">
    <div class="text-center p-t-20 p-b-20">
        <span class="db"><img src="{{asset('_admin/assets/images/logo.png')}}" alt="logo" class="light-logo"/></span>
    </div>
{{--    @include('admin.layouts.alert')--}}
    <form class="form-horizontal m-t-20" id="loginform"  method="POST" action="{{ route('admin.login.store') }}">
        @csrf
        <div class="row p-b-30">
            <div class="col-12">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-user"></i></span>
                    </div>
                    <input id="email" type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="البريد الإلكتروني">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-warning text-white" id="basic-addon2"><i class="ti-lock"></i></span>
                    </div>
                    <input id="password" type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="كلمة المرور">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row border-top border-secondary">
            <div class="col-12">
                <div class="form-group">
                    <div class="p-t-20">
                        <button class="btn btn-info float-left" id="to-recover" type="button"><i class="fa fa-lock m-r-5"></i> نسيت كلمة المرور ؟</button>
                        <button class="btn btn-success " type="submit">تسجيل الدخول</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

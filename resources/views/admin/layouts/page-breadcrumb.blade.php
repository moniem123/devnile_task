<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">لوحة التحكم</h4>
            <div class="mr-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">الرئيسية</a></li>
                        @isset($routes)
                            @foreach($routes as $route)
                                <li class="breadcrumb-item"><a href="{{$route['route']}}">{{$route['name']}}</a></li>
                            @endforeach
                        @endisset
                        <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<!DOCTYPE html>
<html dir="rtl" lang="ar">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('_admin/assets/images/favicon.png')}}">
    <title> لوحة تحكم الموقع - @yield('title')</title>
    <link href="{{asset('_admin/assets/libs/flot/css/float-chart.css')}}" rel="stylesheet">
    <link href="{{asset('_admin/dist/css/style.min.css')}}" rel="stylesheet">
    <link href="{{asset('_admin/dist/css/style-rtl.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('_admin/assets/libs/quill/dist/quill.snow.css') }}">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <style>
        .display-img-validation{
            display: block;
        }
        .btnDelete{
            text-decoration: none;
        }
        .btnDelete:hover{
            text-decoration: none;
            background: #fff;
            color: #da542e;
        }
        .btnDelete i{
            text-decoration: none;
            color: #da542e;
        }
        .btnDelete i:hover{
            text-decoration: none;
            color: #da542e;
        }
         #notyBtn1{
             margin: 4px;
         }
        #notyBtn2{
            margin: 4px;
        }

    </style>
    @yield('styles')
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<body>
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<div id="main-wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->


    @include('admin.layouts.header')

    @include('admin.layouts.sidebar')

    <div class="page-wrapper">
        @yield('breadcrumb')

        <div class="container-fluid">

        @include('admin.layouts.alert')
        @yield('content')

        </div>
        <footer class="footer text-center">
            جميع الحقوق محفوظة  <a href="#">للشركة</a> @ {{date('Y')}}.
        </footer>
    </div>
</div>
@include('admin.layouts.scripts')
</body>

</html>

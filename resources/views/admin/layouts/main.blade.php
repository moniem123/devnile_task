@extends('admin.layouts.app')
@section('content')
<div class="row">
    <div class="col-md-6 col-lg-4 col-xl-3">
        <div class="card card-hover">
            <a href="#" class="box bg-cyan text-center d-block">
                <h1 class="font-light text-white"><i class="mdi mdi-human"></i></h1>
                <h6 class="text-white"><span class="text-white">المشرفين ( {{\App\Models\Supervisor::count()}} )</span></h6>
            </a>
        </div>
    </div>

    <div class="col-md-6 col-lg-4 col-xl-3">
        <div class="card card-hover">
            <a href="#" class="box bg-warning text-center d-block">
                <h1 class="font-light text-white"><i class="mdi mdi-chart-areaspline"></i></h1>
                <h6 class="text-white"><span class="text-white">قسم ( {{\App\Models\Category::count()}} )</span></h6>
            </a>
        </div>
    </div>

    <div class="col-md-6 col-lg-4 col-xl-3">
        <div class="card card-hover">
            <a href="#" class="box bg-info text-center d-block">
                <h1 class="font-light text-white"><i class="mdi mdi-blinds"></i></h1>
                <h6 class="text-white"><span class="text-white">منتج ( {{\App\Models\Product::count()}} )</span></h6>
            </a>
        </div>
    </div>

</div>

@endsection

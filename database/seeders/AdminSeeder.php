<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::updateOrCreate(['email'=>'admin@gmail.com',],[
            'name'=>'admin',
            'email'=>'admin@gmail.com',
            'phone'=>123456,
            'password'=>123456,
            'is_active'=>1,
        ]);
    }
}

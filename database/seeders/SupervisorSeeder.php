<?php

namespace Database\Seeders;

use App\Models\Supervisor;
use Illuminate\Database\Seeder;

class SupervisorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Supervisor::updateOrCreate(['email'=>'supervisor@gmail.com',],[
            'name'=>'supervisor',
            'email'=>'supervisor@gmail.com',
            'phone'=>123456,
            'password'=>123456,
            'is_active'=>1,
        ]);
    }
}

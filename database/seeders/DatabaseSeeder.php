<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Seeder;
use App\Models\Admin;
use App\Models\User;
use App\Models\Supervisor;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AdminSeeder::class,
            SupervisorSeeder::class
        ]);
//        User::factory(10)->create();
//        Admin::factory(10)->create();
//        Supervisor::factory(10)->create();
//        Category::factory(10)->create();
//        Product::factory(10)->create();
    }
}

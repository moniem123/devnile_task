<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $category = Category::inRandomOrder()->first();
        return [
            'name'=>$this->faker->name,
            'description'=>$this->faker->text,
            'slug'=>$this->faker->unique()->slug,
            'category_id'=>$category->id,
        ];
    }
}

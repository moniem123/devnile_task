<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SupervisorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'=>'required|string',
            'email'=>'required|email:filter|unique:supervisors,email',
            'phone'=>'required|numeric|unique:supervisors,phone',
            'password'=>'required|string|min:6|confirmed',
            'avatar'=>'required|file',
        ];
        if($this->method() == 'PUT' || $this->method() == 'PATCH'){
            $rules['email'] = 'required|email:filter|unique:supervisors,email,'.$this->supervisor->id;
            $rules['phone'] = 'required|numeric|unique:supervisors,phone,';
            $rules['password'] = 'sometimes|nullable|string|min:6|confirmed';
            $rules['avatar'] =' sometimes|nullable|file';
        }
        return  $rules;
    }
}

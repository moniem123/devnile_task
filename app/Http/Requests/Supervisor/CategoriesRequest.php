<?php

namespace App\Http\Requests\Supervisor;

use Illuminate\Foundation\Http\FormRequest;

class CategoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'name'=>'required|string',
            'slug'=>'required|string|unique:categories,slug',
            'icon'=>'required|string',
        ];
        if($this->method() == 'PUT' || $this->method() == 'PATCH'){
            $rules['slug'] = 'required|string|unique:categories,slug,'.$this->category->id;
        }
        return $rules;
    }
}

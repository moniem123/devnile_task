<?php

namespace App\Http\Requests\Supervisor;

use Illuminate\Foundation\Http\FormRequest;

class ProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        dd(\request()->all());
        $rules =  [
            'name'=>'required|string|min:5|max:70',
            'description'=>'required|string|min:10|max:200',
            'category_id'=>'required|numeric|exists:categories,id',
            'slug'=>'required|string|unique:products,slug',
            'image'=>'required|file',
            'images'=>'sometimes|array',
            'images.*'=>'required_with:images|file',
        ];
        if($this->method() == 'PUT' || $this->method() == 'PATCH'){
            $rules['slug'] = 'required|string|unique:products,slug,'.$this->product->id;
            $rules['image'] = 'sometimes|nullable|file';
        }
        return  $rules;
    }
}

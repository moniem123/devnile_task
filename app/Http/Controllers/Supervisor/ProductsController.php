<?php

namespace App\Http\Controllers\Supervisor;

use App\Http\Controllers\Controller;
use App\Http\Requests\Supervisor\ProductsRequest;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(10);
        return view('supervisor.products.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('supervisor.products.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProductsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductsRequest $request)
    {
        $product = Product::create($request->except(['_token','images']));
        if($request->has('images')){
            multiUploader($request,'images','product_images/'.date('Y-m'),new ProductImage(),$product->id);
        }
        return redirect()->route('supervisor.products.index')->with('success',__('added successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return  view('supervisor.products.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return  view('supervisor.products.edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductsRequest  $request
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductsRequest $request, Product $product)
    {
        $product->update($request->except(['_token','_methods','images']));
        if($request->has('images')) {
            multiUploader($request, 'images', 'product_images/' . date('Y-m'), new ProductImage(), ['product_id'=>$product->id]);
        }
        return redirect()->route('supervisor.products.index')->with('success',__('updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return response()->json(['status'=>true,'message'=>__('deleted successfully')],200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete_selected(Request $request){
        Product::whereIn('id',$request['ids'])->delete();
        return response()->json(['status'=>true,'message'=>__('deleted successfully')],200);
    }

    public function imgDelete($id){
        $product_image = ProductImage::findOrFail($id);
        $product = $product_image->product;
        $product_image->delete();

        return redirect()->route('supervisor.products.show',compact('product'))->with('success',__('deleted successfully'));
    }
}

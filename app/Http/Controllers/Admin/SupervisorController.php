<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SupervisorRequest;
use App\Models\Supervisor;
use Illuminate\Http\Request;

class SupervisorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supervisors = Supervisor::paginate(10);
        return view('admin.supervisor.index',compact('supervisors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.supervisor.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SupervisorRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SupervisorRequest $request)
    {
        $inputs = $request->except(['_token']);
        $inputs['is_active'] = 1;
        Supervisor::create($inputs);
        return redirect()->route('admin.supervisor.index')->with('success',__('added successfully'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Supervisor  $supervisor
     * @return \Illuminate\Http\Response
     */
    public function show(Supervisor $supervisor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Supervisor  $supervisor
     * @return \Illuminate\Http\Response
     */
    public function edit(Supervisor $supervisor)
    {
        return  view('admin.supervisor.edit',compact('supervisor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SupervisorRequest  $request
     * @param  \App\Models\Supervisor  $supervisor
     * @return \Illuminate\Http\Response
     */
    public function update(SupervisorRequest $request, Supervisor $supervisor)
    {
        $supervisor->update($request->except(['_token','_method']));
        return redirect()->route('admin.supervisor.index')->with('success',__('updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Supervisor  $supervisor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supervisor $supervisor)
    {
        $supervisor->delete();
        return response()->json(['status'=>true,'message'=>__('deleted successfully')],200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete_selected(Request $request){
        Supervisor::whereIn('id',$request['ids'])->delete();
        return response()->json(['status'=>true,'message'=>__('deleted successfully')],200);
    }
    public function activeToggle(Supervisor $supervisor){
        if($supervisor->is_active){
            $supervisor->update(['is_active'=>0]);
            return response()->json(['status'=>true,'message'=>__('supervisor deactivate'),'flag'=>0],200);
        }else{
            $supervisor->update(['is_active'=>1]);
            return response()->json(['status'=>true,'message'=>__('supervisor activate'),'flag'=>1],200);
        }

    }
}

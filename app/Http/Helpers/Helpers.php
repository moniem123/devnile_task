<?php
use App\Models\Category;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Schema;

if(!function_exists('categories')){
    /**
     * @return mixed
     */
    function categories(){
        $categories = Category::get()->mapWithKeys(function ($q){
            return[$q['id']=>$q['name']];
        });
        return $categories;
    }
}

if (!function_exists('getImg')){
    /**
     * @param $filename
     * @return string
     */
    function getImg($filename){
        if (!empty($filename)) {
            $base_url = url('/');
            return $base_url . '/storage/' . $filename;
        } else {
            return '';
        }
    }
}

if(!function_exists('uploadpath')){
    /**
     * @param $folder
     * @return string
     */
    function uploadpath($folder){
        return 'photos/' . $folder;
    }

}

if(!function_exists('uploader')){
    /**
     * @param $file
     * @param string $folder
     * @return false|string
     */
    function uploader($file, $folder = ''){
        $path = Storage::disk('public')->putFile(uploadPath($folder), $file);
        return $path;
    }

}

if(!function_exists('checkAttrExists')){
    /**
     * @param $model
     * @param $attr
     * @return mixed
     */
    function checkAttrExists($model,$attr){
        return Schema::hasColumn($model->getTable(), $attr);
    }
}

if(!function_exists('multiUploader')){
    /**
     * @param $request
     * @param $img_name
     * @param $folder
     * @param $model
     * @param $onId
     * @return bool
     */
    function multiUploader($request,$img_name,$folder,$model,$onId=null){
        foreach ($request[$img_name] as $image){
            $filename = rand(99999, 99999999) . $image->getClientOriginalName();
            $path = Storage::disk('public')->putFile(uploadpath($folder), $image);
            if(checkAttrExists($model,'image'))
                $model->create(['image' => $path] + $onId);
            if(checkAttrExists($model,'attachment'))
                $model->create(['attachment' => $path] + $onId);
        }
        return true;
    }

}


<?php

namespace App\Http\Traits;

trait FilesAttributes
{

    /**
     * @return null|string
     */
    public function getImageAttribute(){
        if(isset($this->attributes['image'])){
            if(strpos($this->attributes['image'],'https') !== false)
                return $this->attributes['image'];
            return getImg($this->attributes['image']);
        }else{
            return '';
        }
    }

    /**
     * @param $value
     */
    public function setImageAttribute($value){
        if (!empty($value)){
            if (is_string($value)) {
                $this->attributes['image'] = $value;
            } else {
                $this->attributes['image'] = uploader($value, $this->folder.'/'.date('Y-m'));
            }
        }
    }

    /**
     * @return null|string
     */
    public function getAvatarAttribute(){
        if(isset($this->attributes['avatar'])){
            if(strpos($this->attributes['avatar'],'https') !== false)
                return $this->attributes['avatar'];
            return getImg($this->attributes['avatar']);
        }else{
            return '';
        }
    }

    /**
     * @param $value
     */
    public function setAvatarAttribute($value){
        if (!empty($value)){
            if (is_string($value)) {
                $this->attributes['avatar'] = $value;
            } else {
                $this->attributes['avatar'] = uploader($value, $this->folder.'/'.date('Y-m'));
            }
        }
    }
}
